(GraphicsLayer
  (bounds 800.00 600.00)
  (children 1
    (GraphicsLayer
      (bounds 800.00 600.00)
      (contentsOpaque 1)
      (drawsContent 1)
      (children 7
        (GraphicsLayer
          (position 120.00 0.00)
          (bounds 50.00 50.00)
          (contentsOpaque 1)
          (drawsContent 1)
          (backgroundColor #0000FF)
        )
        (GraphicsLayer
          (position 0.00 60.00)
          (bounds 50.00 50.00)
          (contentsOpaque 1)
          (drawsContent 1)
          (hasGpuRasterizationHint 1)
          (backgroundColor #008000)
        )
        (GraphicsLayer
          (position 60.00 60.00)
          (bounds 50.00 50.00)
          (contentsOpaque 1)
          (drawsContent 1)
          (hasGpuRasterizationHint 1)
          (backgroundColor #008000)
        )
        (GraphicsLayer
          (position 120.00 60.00)
          (bounds 50.00 50.00)
          (contentsOpaque 1)
          (drawsContent 1)
          (hasGpuRasterizationHint 1)
          (backgroundColor #008000)
        )
        (GraphicsLayer
          (position 0.00 120.00)
          (bounds 50.00 50.00)
          (contentsOpaque 1)
          (drawsContent 1)
          (hasGpuRasterizationHint 1)
          (backgroundColor #008000)
        )
        (GraphicsLayer
          (position 60.00 120.00)
          (bounds 50.00 50.00)
          (contentsOpaque 1)
          (drawsContent 1)
          (hasGpuRasterizationHint 1)
          (backgroundColor #008000)
        )
        (GraphicsLayer
          (position 120.00 120.00)
          (bounds 50.00 50.00)
          (contentsOpaque 1)
          (drawsContent 1)
          (hasGpuRasterizationHint 1)
          (backgroundColor #008000)
        )
      )
    )
  )
)

