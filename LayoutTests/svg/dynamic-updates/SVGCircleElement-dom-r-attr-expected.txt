SVG 1.1 dynamic update tests

Tests dynamic updates of the 'r' attribute of the SVGCircleElement object

On success, you will see a series of "PASS" messages, followed by "TEST COMPLETE".


PASS circleElement.getAttribute('r') is "1"
PASS circleElement.getAttribute('r') is "150"
PASS successfullyParsed is true

TEST COMPLETE
(repaint rects
  (rect 157 157 2 2)
  (rect 8 8 300 300)
)

