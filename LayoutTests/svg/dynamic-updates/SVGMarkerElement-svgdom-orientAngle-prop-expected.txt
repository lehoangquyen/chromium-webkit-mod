SVG 1.1 dynamic update tests

Tests dynamic updates of the 'orientAngle' property of the SVGMarkerElement object

On success, you will see a series of "PASS" messages, followed by "TEST COMPLETE".


PASS markerElement.orientType.baseVal is SVGMarkerElement.SVG_MARKER_ORIENT_ANGLE
PASS markerElement.orientAngle.baseVal.value is 45
PASS markerElement.orientType.baseVal is SVGMarkerElement.SVG_MARKER_ORIENT_ANGLE
PASS markerElement.orientAngle.baseVal.value is 0
PASS successfullyParsed is true

TEST COMPLETE
(repaint rects
  (rect 8 8 2 2)
  (rect 123 128 80 80)
  (rect 128 133 70 70)
)

