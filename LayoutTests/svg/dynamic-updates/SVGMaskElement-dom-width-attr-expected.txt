SVG 1.1 dynamic update tests

Tests dynamic updates of the 'width' attribute of the SVGMaskElement object

On success, you will see a series of "PASS" messages, followed by "TEST COMPLETE".


PASS maskElement.getAttribute('width') is "10%"
PASS maskElement.getAttribute('width') is "100%"
PASS successfullyParsed is true

TEST COMPLETE
(repaint rects
  (rect 30 30 151 151)
  (rect 30 30 151 151)
)

