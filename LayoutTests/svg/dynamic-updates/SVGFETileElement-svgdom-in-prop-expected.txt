SVG 1.1 dynamic update tests

Tests dynamic updates of the 'in' property of the SVGFETileElement object

On success, you will see a series of "PASS" messages, followed by "TEST COMPLETE".


PASS tile.in1.baseVal is "SourceGraphic"
PASS tile.in1.baseVal is "offset"
PASS successfullyParsed is true

TEST COMPLETE
(repaint rects
  (rect 22 47 352 252)
  (rect 23 48 355 250)
)

