SVG 1.1 dynamic update tests

Tests dynamic updates of the 'markerUnits' attribute of the SVGMarkerElement object

On success, you will see a series of "PASS" messages, followed by "TEST COMPLETE".


PASS markerElement.getAttribute('markerUnits') is "userSpaceOnUse"
PASS markerElement.getAttribute('markerUnits') is "strokeWidth"
PASS successfullyParsed is true

TEST COMPLETE
(repaint rects
  (rect 8 8 2 2)
  (rect 137 138 56 56)
  (rect 128 133 70 70)
)

