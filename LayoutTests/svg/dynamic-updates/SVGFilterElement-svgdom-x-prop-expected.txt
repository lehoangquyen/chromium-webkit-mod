SVG 1.1 dynamic update tests

Tests dynamic updates of the 'x' property of the SVGFilterElement object

On success, you will see a series of "PASS" messages, followed by "TEST COMPLETE".


PASS filterElement.x.baseVal.value is 100
PASS filterElement.x.baseVal.value is 0
PASS successfullyParsed is true

TEST COMPLETE
(repaint rects
  (rect 108 8 200 200)
  (rect 8 8 200 200)
)

