SVG 1.1 dynamic update tests

Tests dynamic updates of the 'patternUnits' attribute of the SVGPatternElement object

On success, you will see a series of "PASS" messages, followed by "TEST COMPLETE".


PASS patternElement.getAttribute('patternUnits') is null
PASS patternElement.getAttribute('patternUnits') is "userSpaceOnUse"
PASS successfullyParsed is true

TEST COMPLETE
(repaint rects
  (rect 18 18 190 190)
  (rect 8 8 50 50)
  (rect 58 8 50 50)
)

