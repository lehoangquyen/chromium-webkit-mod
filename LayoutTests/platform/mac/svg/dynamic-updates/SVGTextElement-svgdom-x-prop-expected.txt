SVG 1.1 dynamic update tests

Text content
Tests dynamic updates of the 'x' property of the SVGTextElement object

On success, you will see a series of "PASS" messages, followed by "TEST COMPLETE".


PASS textElement.x.baseVal.getItem(0).value is 50
PASS textElement.x.baseVal.getItem(0).value is 0
PASS successfullyParsed is true

TEST COMPLETE
(repaint rects
  (rect 58 14 79 18)
  (rect 8 14 79 18)
)

