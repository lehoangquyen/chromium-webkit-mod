SVG 1.1 dynamic update tests

Test passed
Tests dynamic updates of the 'transform' attribute of the SVGTextElement object

On success, you will see a series of "PASS" messages, followed by "TEST COMPLETE".


PASS textElement.getAttribute('transform') is null
PASS textElement.getAttribute('transform') is "translate(0,-200)"
PASS successfullyParsed is true

TEST COMPLETE
(repaint rects
  (rect 8 208 72 19)
  (rect 8 8 72 19)
)

